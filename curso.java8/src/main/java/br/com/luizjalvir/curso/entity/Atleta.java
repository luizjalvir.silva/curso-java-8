package br.com.luizjalvir.curso.entity;

import java.util.List;
import java.util.stream.Collectors;

public class Atleta {
	String nome;

	public Atleta(String nome) {
		this.nome = nome;
	}

	public static Atleta of(String nome) {
		return new Atleta(nome);
	}

	public static List<Atleta> of(List<String> nomes) {
		return nomes.stream().map(Atleta::of).collect(Collectors.toList());
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
