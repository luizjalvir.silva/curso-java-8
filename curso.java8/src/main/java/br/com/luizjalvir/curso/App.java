package br.com.luizjalvir.curso;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import br.com.luizjalvir.curso.entity.Atleta;
import br.com.luizjalvir.curso.entity.Equipe;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		List<Equipe> equipes = new ArrayList<Equipe>();

		Equipe chapecoense = Equipe.of("Chapecoense", 10,
				Atleta.of(Stream.of("Tiepo", "Eduardo", "Gum", "Rafael Pereira").collect(Collectors.toList())));
		equipes.add(chapecoense);

		Equipe avai = Equipe.of("Avai", 15,
				Atleta.of(Stream.of("Cláudio Vitor", "Gledson", "Lucas Frigeri", "Léo Lopes").collect(Collectors.toList())));
		equipes.add(avai);

		equipes.stream().filter(e -> e.getNome().startsWith("C")).forEach(e -> System.out.println(e.getQuantidadeTitulos()));

		equipes.stream().filter(e -> e.getNome().startsWith("A"))
				.forEach(e -> e.getAtletas().stream().forEach(a -> System.out.println(a.getNome())));

	}
}
