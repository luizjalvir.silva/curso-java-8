package br.com.luizjalvir.curso.entity;

import java.util.List;

public class Equipe {
	private String nome;
	private int quantidadeTitulos;
	private List<Atleta> atletas;
	
	
	public Equipe(String nome, int quantidadeTitulos, List<Atleta> atletas) {
		this.nome = nome;
		this.quantidadeTitulos = quantidadeTitulos;
		this.atletas = atletas;
	}
	public static Equipe of(String nome, int quantidadeTitulos, List<Atleta> atletas) {
		return new Equipe(nome,quantidadeTitulos,atletas);
	}
	@Override
	public String toString() {
		return "Equipe [nome=" + nome + ", quantidadeTitulos=" + quantidadeTitulos + ", atletas=" + atletas + "]";
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getQuantidadeTitulos() {
		return quantidadeTitulos;
	}
	public void setQuantidadeTitulos(int quantidadeTitulos) {
		this.quantidadeTitulos = quantidadeTitulos;
	}
	public List<Atleta> getAtletas() {
		return atletas;
	}
	public void setAtletas(List<Atleta> atletas) {
		this.atletas = atletas;
	}
	
}
